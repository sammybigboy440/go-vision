package segmentation

import (
	"image"
	"math"
	"sort"
)

type Segmentation struct {
	_img image.Image
}

func NewSegmentation(img image.Image) Segmentation {
	return Segmentation{_img: img}
}

//generate a threshold of image with the number of layers
func (i *Segmentation) Threshold(layer int) *image.Gray {
	getMean := func(pix []uint8) (mean uint8, max uint8) {
		for _, v := range pix {
			max = uint8(math.Max(float64(max), float64(v)))
			mean = (mean + v) / 2
		}
		return mean, max
	}
	var (
		gray      = i.ToGray(i._img) //convert to gray
		mean, max = getMean(gray.Pix)
		bound     = gray.Bounds()
		layers    = make([]float32, 0)
	)
	//create the layers
	layers = append(layers, float32(float32(mean)/float32(max)))
	if layer < 1 {
		layer = 1
	}
	layerFactor := float32(float32(1) / float32(layer))
	for i := 0; i < (layer - 1); i++ {
		layers = append(layers, float32(layerFactor*float32(i)))
	}
	sort.Slice(layers, func(i, j int) bool { return layers[i] < layers[j] })

	for x := 0; x < bound.Max.X; x++ {
		for y := 0; y < bound.Max.Y; y++ {
			c := gray.GrayAt(x, y)
			_c := uint8(0)
			for i := 0; i < len(layers); i++ {
				if float32(float32(c.Y)/float32(max)) < layers[i] {
					_c = uint8(math.Max(float64((layers[i]-layerFactor)*float32(max)), 0))
					break
				} else {
					_c = uint8(math.Min(float64((layers[i]+layerFactor)*float32(max)), float64(max)))
				}
			}
			c.Y = _c
			gray.Set(x, y, c)
		}
	}
	return gray
}

func (i *Segmentation) ToGray(img image.Image) *image.Gray {
	var (
		bound = img.Bounds()
		gray  = image.NewGray(bound)
	)
	for x := 0; x < bound.Max.X; x++ {
		for y := 0; y < bound.Max.Y; y++ {
			gray.Set(x, y, img.At(x, y))
		}
	}
	return gray
}
