package vision

import (
	"image"
	"os"

	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	"gitlab.com/sammybigboy440/go-vision/segmentation"
)

type vision struct {
	_path string //path to the image
	_img  image.Image
}

func NewVision(path string) (v vision, err error) {
	reader, err := os.Open(path)
	if err != nil {
		return v, err
	}
	img, _, err := image.Decode(reader)
	if err != nil {
		return v, err
	}
	return vision{_path: path, _img: img}, err
}

func (i *vision) Segment() segmentation.Segmentation {
	return segmentation.NewSegmentation(i._img)
}
