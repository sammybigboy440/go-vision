package main

import (
	"flag"
	"image/png"
	"os"

	"gitlab.com/sammybigboy440/go-vision/vision"
)

func main() {
	_img := flag.String("img", "", "-img={image path}. path to the image file")
	flag.Parse()

	if len(*_img) == 0 {
		panic("image path is required")
	}
	v, err := vision.NewVision(*_img)
	if err != nil {
		panic(err)
	}
	sg := v.Segment()

	f, _ := os.Create("test1.png")
	defer f.Close()
	//
	// png.Encode(f, sg.Threshold(4))
	png.Encode(f, sg.EdgeByLaplace())
}
