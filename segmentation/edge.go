package segmentation

import (
	"image"
	"image/color"
)

func (i *Segmentation) EdgeByLaplace() *image.RGBA {
	kernel := [][]int{{1, 1, 1}, {1, -8, 1}, {1, 1, 1}}
	lenX := len(kernel[0])
	lenY := len(kernel)
	var (
		img   = i._img
		bound = img.Bounds()
		//extend
		src = image.NewRGBA(image.Rect((bound.Min.X + lenX), (bound.Min.Y + lenY), (bound.Max.X + lenX), (bound.Max.Y + lenY)))
		dst = image.NewRGBA(bound)
	)
	//copy image to the blank source when using padding approach
	for x := 0; x < bound.Max.X; x++ {
		for y := 0; y < bound.Max.Y; y++ {
			src.Set(x, y, img.At(x, y))
		}
	}
	//begin convolution on padded image
	for x := 0; x < bound.Max.X; x++ {
		for y := 0; y < bound.Max.Y; y++ {
			//RGB sums
			var r, g, b float64
			//run the metrix
			for mx := 0; mx < lenX; mx++ {
				for my := 0; my < lenY; my++ {
					mval := kernel[mx][my]
					_r, _g, _b, _ := src.At(x+mx, y+my).RGBA()

					r += float64(mval * int(_r))
					g += float64(mval * int(_g))
					b += float64(mval * int(_b))
					// a += float64(mval * int(_a))
				}
			}
			dst.Set(x, y, color.RGBA{uint8(r), uint8(g), uint8(b), 100})
		}
	}
	return dst
}
